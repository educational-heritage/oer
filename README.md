# Educational Heritage - Open Educational Resources

Streamline our development environment.

Follow this instructions to set up your development environment in three simple steps.

Check that you system meets our current requirements:

``` bash
$ git --version
git version 1.9.1

$ make --version
GNU Make 3.81

$ docker --version
Docker version 17.09.0-ce, build afdb6d4

$ docker-compose --version
docker-compose version 1.13.0, build 1719ceb
```

Get the source code:

``` bash
$ git clone --recursive git@gitlab.com:educational-heritage/oer.git
```

Run the tests to verify that everything works properly:

``` bash
$ cd oer
$ make test
```

That's all. At this stage, you have everything set up and running on your local machine.

``` bash
$ http 127.0.0.1:6792
$ browser 127.0.0.1:6793
```

Follow our [Contributing Guide](./CONTRIBUTING.md) to get a deep understanding about our environments.
