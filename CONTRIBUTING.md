# Notes to contribute 

Edit the /etc/hosts file to include:

``` bash
127.0.0.1       api.legadoeducativo.local
127.0.0.1       www.legadoeducativo.local
127.0.0.1       mongo.legadoeducativo.local
```

Run all development containers:

``` bash
$ make start
```
