default: help

.PHONY: help
help:
	@echo "help - Show this help"
	@echo "stop - Stop containers"
	@echo "test - Run all tests"
	@echo "start - Run development environment"
	@echo "build - Show running containers"
	@echo "status - Show containers"

.PHONY: stop
stop:
	docker-compose down --volumes

.PHONY: test
test: stop
test:
	docker-compose -f docker-compose.yml -f docker-compose.test.yml up oer_api_test

.PHONY: start
start: stop
start:
	docker-compose up -d

.PHONY: build
build: stop
build:
	docker-compose up -d --build

.PHONY: status
status:
	docker-compose ps
